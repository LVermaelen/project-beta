import react, {useState, useEffect} from 'react';

const EmployeeSales = () => {
    const [loading, setLoading] = useState(false);
    const [employees, setEmployees] = useState([]);
    const[employeeSales, setSales] = useState([])
    const url = "http://localhost:8090/api/employees/";

    const fetchEmployees = async () => {
        try {
            const response = await fetch(url);

            if (response.ok) {
                const theJson = await response.json();


                setLoading(false)
                setEmployees(theJson.sales_people)
            }
        } catch (e) {
            console.log("error", e)
        }
    };
    useEffect(() => {
        setLoading(true);
        fetchEmployees();
    }, [])

    const useHandleSelect = (e) => {
        const value = e.target.value

        const name = e.target.name

        const salesUrl = `http://localhost:8090${value}sales/`
        const fetchEmployeeSales = async () => {
            try {
                const response = await fetch(salesUrl);
                if (response.ok) {
                    const theJson = await response.json();

                    setSales(theJson.sales)
                }
            } catch (e) {
                console.log("error", e)
            }
        };
        fetchEmployeeSales()
    }

    if (employeeSales === []) {
        return (
            <>
            <h1>Employee Sales</h1>
            <select onChange={useHandleSelect} value={employees.sales_person} placeholder="Sales Employee" name="sales_person" id="sales_person">
                <option value="">Choose a Sales Employee</option>
                {employees.map((sales_person) => { return (
                    <option key={[sales_person.href]} value={[sales_person.href]}>{sales_person.name}</option>
                )})}
            </select>
            </>

        )
    } else {
        return (
            <>
            <h1>Employee Sales</h1>
            <select onChange={useHandleSelect} value={employees.sales_person} placeholder="Sales Employee" name="sales_person" id="sales_person">
                <option value="">Choose a Sales Employee</option>
                {employees.map((sales_person) => { return (
                    <option key={[sales_person.href]} value={[sales_person.href]}>{sales_person.name}</option>
                )})}
            </select>

            <div className='container-fluid'>
                {loading &&(
                    <div>
                        {" "}
                        <h1>Loading...</h1>
                    </div>
                )}
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Employee ID</th>
                    <th>Employee Name</th>
                    <th>Customer</th>
                    <th>Vin #</th>
                    <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {employeeSales.map((sale) => {
                    return (
                        <tr key={sale.href}>
                        <td>{sale.sales_person.employee_number}</td>
                        <td>{sale.sales_person.name}</td>
                        <td>{sale.customer.name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>{"$" + sale.price}</td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
            </>
        );
    }
}

export default EmployeeSales;
