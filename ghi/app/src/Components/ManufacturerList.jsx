import react, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

export const ManufacturerList = () => {
  const [loading, setLoading] = useState(false);
  const [manufacturers, setManufacturers] = useState([])
  const url = "http://localhost:8100/api/manufacturers/"

  const fetchData = async () => {
    try {
        const response = await fetch(url);
        if (response.ok) {
            const theJson = await response.json();

            setLoading(false)
            setManufacturers(theJson.manufacturers)
        }
    } catch (e) {
        console.log("error", e)
    }
  };

  useEffect(() => {
    setLoading(true);
    fetchData();
  }, [])


  return (
    <div className='container-fluid p-2'>
      <div className='d-grid gap-2 d-md-flex justify-content-md-center'>
        <Link className="btn btn-outline-success g-2" to="/manufacturers/new">Create a Manufacturer</Link>
      </div>
      {loading &&(
          <div>
              {" "}
              <h1>Loading...</h1>
          </div>
      )}
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
            <tr>
            <th>Manufacturer Name</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers.map((manufacturer) => {
            return (
                <tr key={manufacturer.href}>
                <td>{manufacturer.name}</td>
                </tr>
            );
            })}
        </tbody>
      </table>
    </div>
  )
}
