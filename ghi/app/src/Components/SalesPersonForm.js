import React, { useState } from "react";

export const SalesPersonForm = () => {
    const [sales_person, setSalesPerson] = useState({
        name: '',
        employee_number: 0
    })

    const handleInputChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setSalesPerson({ ...sales_person, [name]: value });
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {...sales_person };

        await fetch("http://localhost:8090/api/employees/", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"}
        }).then((e) => {
            window.location.href = "/sales"
        })
    }

    return (
        <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Add a New Sales Employee</h1>

            <form onSubmit={handleSubmit} className='form'>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={sales_person.name}
                  placeholder='Name'
                  required
                  type='text'
                  name='name'
                  id='name'
                  className='form-control'
                />
                <label htmlFor='name'>Name</label>
              </div>
              <div className='mb-3'>
                <label htmlFor='employee_id'>Employee ID</label>
                <input
                  onChange={handleInputChange}
                  value={sales_person.employee_number}
                  placeholder='Employee ID'
                  required
                  type='number'
                  name='employee_number'
                  id='employee_number'
                  className='form-control'
                />
              </div>
              <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                <button className='btn btn-outline-success'>Add</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}
