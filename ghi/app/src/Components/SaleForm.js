import React, { useState, useEffect } from "react";

export const SaleForm = () => {
    const [sale, setSale] = useState({
        sales_person: "",
        customer: "",
        automobile: "",
        price: 0,
        sales_people:[],
        customers: [],
        automobiles: []
    })

    const handleInputChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setSale({ ...sale, [name]: value });
      };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {...sale };
        delete data.sales_people;
        delete data.customers;
        delete data.automobiles;
        await fetch("http://localhost:8090/api/sales/", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"}
        }).then((e) => {
            window.location.href = "/sales"
        })
    }

    const fetchEmployees = async () => {
        const url = "http://localhost:8090/api/employees/"
        try {
            const response = await fetch(url);

            if (response.ok) {
                const theJson = await response.json();

                setSale((sp) => ({... sp, sales_people: theJson.sales_people}))

            }
        } catch (e) {
            console.log("error", e)
        }
    }

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/"
        try {
            const response = await fetch(url);

            if (response.ok) {
                const theJson = await response.json();

                setSale((c) => ({...c, customers: theJson.customers}))

            }
        } catch (e) {
            console.log("error", e)
        }
    }
    const fetchAutomobiles = async () => {
        const url = "http://localhost:8090/api/automobiles/unsold"
        try {
            const response = await fetch(url);

            if (response.ok) {
                const theJson = await response.json();

                setSale((a) => ({...a, automobiles: theJson.automobiles}))

            }
        } catch (e) {
            console.log("error", e)
        }
    };
    useEffect(() => {
        fetchEmployees();
        fetchCustomers();
        fetchAutomobiles();

    }, [])

  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create a New Sale</h1>

            <form onSubmit={handleSubmit} className='form'>
            <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={sale.sales_person}
                  placeholder='Employee'
                  name='sales_person'
                  id='sales_person'
                  className='form-control'
                >
                  <option value=''>Choose an Employee</option>
                  {sale.sales_people.map((sales_person) => {
                    return (
                      <option key={[sales_person.id]} value={[sales_person.id]}>
                        {sales_person.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={sale.customer}
                  placeholder='Customer'
                  name='customer'
                  id='customer'
                  className='form-control'
                >
                  <option value=''>Choose a Customer</option>
                  {sale.customers.map((customer) => {
                    return (
                      <option key={[customer.id]} value={[customer.id]}>
                        {customer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={sale.automobile}
                  placeholder='Vin'
                  name='automobile'
                  id='automobile'
                  className='form-control'
                >
                  <option value=''>Choose a Vin</option>
                  {sale.automobiles.map((automobile) => {
                    return (
                      <option key={[automobile.import_href]} value={[automobile.import_href]}>
                        {automobile.vin}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={sale.price}
                  placeholder='Picture'
                  required
                  type='number'
                  name='price'
                  id='price'
                  className='form-control'
                />
                <label htmlFor='price'>Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}
