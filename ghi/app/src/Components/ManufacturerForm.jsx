import react, {useState, useEffect} from 'react';

export const ManufacturerForm = () => {
    const [manufacturer, setManufacturer] = useState({
        name: ""
    })

    const handleInputChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setManufacturer({ ...manufacturer, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {...manufacturer };

        await fetch("http://localhost:8100/api/manufacturers/", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"}
        }).then((e) => {
            window.location.href = "/manufacturers"
        })
    }

    return (
        <div className='container-fluid'>
          <div className='row'>
            <div className='offset-3 col-6'>
              <div className='shadow p-4 mt-4'>
                <h1>Add a New Manufacturer</h1>

                <form onSubmit={handleSubmit} className='form'>
                  <div className='form-floating mb-3'>
                    <input
                      onChange={handleInputChange}
                      value={manufacturer.name}
                      placeholder='Name'
                      required
                      type='text'
                      name='name'
                      id='name'
                      className='form-control'
                    />
                    <label htmlFor='name'>Name</label>
                  </div>
                  <div className='d-grid gap-2 d-md-flex justify-content-md-end'>
                    <button className='btn btn-outline-success'>Add</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    )
    }
