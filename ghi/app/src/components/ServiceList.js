import React, { useState, useEffect } from "react";

const ServiceList = () => {
  const [services, setService] = useState([]);
  const url = "http://localhost:8080/api/services/";


  const fetchService = async () => {
    try {
      const response = await fetch(url);
      const content = await response.json();
      if (response.ok) {
        const filterService = content.services.filter(
          (service) => service.is_finished === false
        );
        setService(filterService);
      }
    } catch (e) {
      console.log("error", e);
    }
    
  };

  
  useEffect(() => {
    fetchService();
  }, []);

  const finishService = async (href) => {
    fetch(`http://localhost:8080${href}`, {
      method: "PUT",
      body: JSON.stringify({is_finished: true}),
      headers: { "Content-Type": "application/json" },
    }).then(() => {
      window.location.reload();
    });
  };


  const deletedService = async (href) => {
    fetch(`http://localhost:8080${href}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    }).then(() => {
      window.location.reload();
    });
  };


  return (
    <div className='container-fluid'>
      <h1>Service Appointments</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Customer Name</th>
            <th>Vin</th>
            <th>Technician</th>
            <th>Date</th>
            <th>Time</th>
            <th>Reason</th>
            <th>IS VIP</th>
          </tr>
        </thead>
        <tbody>
          {services.map((service) => (
            <tr key={service.href}>
              <td>{service.customer_name}</td>
              <td>{service.vin}</td>
              <td>{service.technician.name}</td>
              <td>{service.service_date}</td> 
              <td>{service.service_time}</td> 
              <td>{service.reason}</td> 

              {service.is_vip && <td>True</td>}
              {!service.is_vip && <td>False</td>}
 
              <td>
                <button
                  onClick={() => deletedService(service.href)}
                  type='button'
                  className='btn btn-outline-danger'
                >
                  Delete
                </button>
              </td>
              <td>
                  <button
                    onClick={() => finishService(service.href)}
                    type='button'
                    className='btn btn-outline-danger'
                  >
                    Finished
                  </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <a href='http://localhost:3000/service/new'>
        <button className='btn btn-success'> Create Service</button>
      </a>
    </div>
  );
};
export default ServiceList;
