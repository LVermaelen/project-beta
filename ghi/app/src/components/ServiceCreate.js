import React, { useState, useEffect } from "react";

const ServiceCreate = () => {
  const [service, setService] = useState({
    customer_name: "",
    vin: "",
    technician: "",
    service_date: "",
    service_time: "",
    reason: "",
    technician_list: [],
  });

  const technicianListUrl = "http://localhost:8080/api/technicians/";

  const handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setService({ ...service, [name]: value });
  };

  const fetchData = async (e) => {
    try {
      const response = await fetch(technicianListUrl);
      if (response.ok) {
        const data = await response.json();
        console.log("data is: ", data)
        setService ((s) => ({ ...s, technician_list: data.technicians }));
        console.log("service is: ", service);
        console.log("useEffect get data: ", data);
      }
    }catch(e) {
      console.log("error", e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);


    const handleSubmit = async (e) => {
      e.preventDefault();

      const data = {
        customer_name: service.customer_name,
        vin: service.vin,
        technician: service.technician,
        service_date: service.service_date,
        service_time: service.service_time,
        reason: service.reason,
      };
      await fetch(`http://localhost:8080/api/services/`, {
        method: "POST",
        body:JSON.stringify(data),
        headers: {"Content-Type": "application/json"},

      })
      .then(() => {
        setService({...service})
        window.location.href='/service';
      })
    };

  return (
    <div className='container'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create Service Appointement</h1>
            <form onSubmit={handleSubmit} className='form'>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={service.customer_name}
                  placeholder='customer_name'
                  required
                  type='text'
                  name='customer_name'
                  id='customer_name'
                  className='form-control'
                />
                <label htmlFor='customer_name'>Customer</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={service.vin}
                  placeholder='vin'
                  required
                  type='text'
                  name='vin'
                  id='vin'
                  className='form-control'
                />
                <label htmlFor='vin'>vin</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={service.service_date}
                  placeholder='service_date'
                  required
                  type='date'
                  name='service_date'
                  id='service_date'
                  className='form-control'
                />
                <label htmlFor='service_date'>Service Date</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={service.service_time}
                  placeholder='service_time'
                  required
                  type='time'
                  name='service_time'
                  id='service_time'
                  className='form-control'
                />
                <label htmlFor='service_date'>Service Time</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={service.reason}
                  placeholder='reason'
                  required
                  type='text'
                  name='reason'
                  id='reason'
                  className='form-control'
                />
                <label htmlFor='reason'>Reason</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={service.technician}
                  placeholder='Technician'
                  required
                  type='text'
                  name='technician'
                  id='technician'
                  className='form-control'
                >
                  <option value=''>Choose a technician</option>
                  {service.technician_list.map((technician) => {
                    return (
                      <option key={[technician.id]} value={[technician.id]}>
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className='btn btn-outline-success'>Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ServiceCreate;
