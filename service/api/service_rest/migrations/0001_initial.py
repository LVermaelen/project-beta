# Generated by Django 4.0.3 on 2022-09-12 20:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AutomobileVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=50)),
                ('import_href', models.CharField(max_length=200, null=True, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_name', models.CharField(max_length=100)),
                ('service_date', models.DateTimeField()),
                ('service_time', models.DateTimeField()),
                ('technician', models.CharField(max_length=100)),
                ('reason', models.CharField(max_length=500)),
                ('vin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='service', to='service_rest.automobilevo')),
            ],
        ),
    ]
