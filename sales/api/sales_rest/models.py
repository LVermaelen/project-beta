from django.db import models
from django.urls import reverse
# Create your models here.


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.SmallIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_sales_person', kwargs={'pk': self.pk})


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    phone = models.CharField(max_length=12)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_customer', kwargs={'pk': self.pk})

class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Sale(models.Model):
    automobile = models.ForeignKey('AutomobileVO', related_name='sales', on_delete=models.CASCADE)
    sales_person = models.ForeignKey('SalesPerson', related_name='sales', on_delete=models.CASCADE)
    customer = models.ForeignKey('Customer', related_name='sales', on_delete=models.CASCADE)
    price = models.IntegerField()

    def get_api_url(self):
        return reverse('api_show_sale', kwargs={'pk': self.pk})
