from django.urls import path
from .views import (
    api_list_sales_person,
    api_list_customers,
    api_show_sales_person,
    api_show_customer,
    api_list_all_sales,
    api_show_sale,
    api_unsold_automobiles
)


urlpatterns = [
    path('employees/', api_list_sales_person, name='api_list_sales_person'),
    path('employees/<int:pk>/', api_show_sales_person, name='api_show_sales_person'),
    path('customers/', api_list_customers, name='api_list_customer'),
    path('customers/<int:pk>/', api_show_customer, name='api_show_customer'),
    path('sales/', api_list_all_sales, name='api_list_all_sales'),
    path('employees/<int:sales_person_id>/sales/', api_list_all_sales, name='api_list_sales'),
    path('sales/<int:pk>', api_show_sale, name='api_show_sale'),
    path('automobiles/unsold/', api_unsold_automobiles, name="api_unsold_automobiles")
]
