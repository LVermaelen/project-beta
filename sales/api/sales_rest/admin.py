from django.contrib import admin
from .models import SalesPerson, Customer, AutomobileVO, Sale
# Register your models here.

admin.site.register([SalesPerson, Sale, Customer, AutomobileVO])
