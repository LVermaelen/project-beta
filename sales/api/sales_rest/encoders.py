from .models import SalesPerson, Sale, Customer, AutomobileVO
from common.json import ModelEncoder

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['import_href', 'vin']


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ['name', 'employee_number', 'id']

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ['name', 'address', 'phone', 'id']

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ['name', 'id']

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ['sales_person', 'automobile', 'customer', 'price']

    encoders = {
        'automobile': AutomobileVODetailEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': CustomerListEncoder()
    }
