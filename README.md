# CarCar

Team:

* Louise Vermaelen - Sales
* Olivia Sun - Service

## Design

  * Full Docker integration (Docker based)
  * Python Django backend
  * REST backend tests based on Insomnia
  * React frontend:
    * JavaScript
    * Hooks


## Sales microservice

  ## Models

  [sales_rest.models.py](./sales/api/sales_rest/models.py)

  * AutomobileVO - this is the connection between our inventory microservice and the sales microservice and is a foreign key on the Sale model

  * Customer - this will be a foreign key on the Sale model

  * SalesPerson - this will be a foreign key on the Sale model

  * Sale - the main model for this microservice that keeps track of the sale of each automobile

  ## Views

  [sales_rest.view.py](./sales/api/sales_rest/views.py)

  * def api_list_sales_person(request): (["GET", "POST"])
    - resposible for listing ad creating a sales person

  * def api_show_sales_person(request, pk): (["GET", "DELETE", "PUT"])
    - responsible for showing the sales person detail, deleting, and updating a sales person

  * def api_list_customers(request): (["GET", "POST"])
    - resposible for listing ad creating a customer

  * def api_show_customer(request, pk): (["GET", "DELETE", "PUT"])
    - responsible for showing the customer detail, deleting, and updating a customer

  * def api_list_all_sales(request, sales_person_id=None): (["GET", "POST"])
    - responsible for listing all sales, listing sales filtered by each employee, and creating a sale

  * def api_show_sale(request, pk): (["GET", "DELETE", "PUT"])
    - responsible for showing the sale detail, deleting, and updating a sale

  * def api_unsold_automobiles(request): (["GET"])
    - responsible for getting all unsold automobiles to populate the dropdown menu in the create a new sale form

  ## Encoders

  [sales_rest.encoders.py](./sales/api/sales_rest/encoders.py)

  ## Frontend (REACT)

  App.js:

  [App.js](./ghi/app/src/App.js)

  Nav.js

  [Nav.js](./ghi/app/src/Nav.js)

  I chose to use function components with hooks for all of my react files.

  Components:

  [SaleList](./ghi/app/src/Components/SalesList.js)
    * handles listing ALL sales and holds the links to all of the following components
  [SaleForm](./ghi/app/src/Components/SaleForm.js)
    * handles creating a new sale
  [SalesPersonForm](./ghi/app/src/Components/SalesPersonForm.js)
    * handles creating a new sales person
  [EmployeeSales](./ghi/app/src/Components/EmployeeSales.js)
    * handles listing sales for each employee
  [CustomerForm](./ghi/app/src/Components/CustomerForm.js)
    * handles creating a new customer

  All components were routed through App.js

  I included a link in the Nav.js to my SaleList.js

  All my other links can be accessed on the SaleList page


## Service microservice

  ## Models:

  [service_rest.models.py](./service/api/service_rest/models.py)

  * AutomobileVO - provides a reach interface between Inventory and Service microservice
  * Technician - defines basic infomation of a technician, is a foreign key of Service
  * Service - main model in service microservice, resposible for appointments list and appointment detail

  ## Views:

  [service_rest.view.py](./service/api/service_rest/views.py)

  * api_list_services: (["GET", "POST"])
    - responsible for listing and creating services.

  * api_show_service: (["DELETE", "GET", "PUT"])
    - responsible for deleting, listing, updating individual service.

  * api_list_technicians: (["GET", "POST"])
    - responsible for listing and creating technicians.

  * api_show_technician: (["DELETE", "GET", "PUT"])
    - responsible for deleting, listing, updating individual technician.

  ## Encoders:

  [service_rest.encoders.py](./service/api/service_rest/encoders.py)

  ## Poller:

  [poller.py](./service/poll/poller.py)

  ## Frontend (REACT)

  Using function components with hooks with less code and better organization.

  App.js:

  [App.js](./ghi/app/src/App.js)

  Nav.js

  [Nav.js](./ghi/app/src/Nav.js)

  Components:

  [ServiceList](./ghi/app/src/Components/ServiceList.js)
    * handles listing ALL services and have delete and finish functions
  [ServiceCreate](./ghi/app/src/Components/ServiceCreate.js)
    * handles creating new service
  [ServiceSearch](./ghi/app/src/Components/ServiceSearch.js)
    * handles searching automobile vin through searching bar
    * show the history of the automobile according to vin number from input field if it had been serviced
    * if input field is empty, then all automobile done service will be listed
  [TechnicianList](./ghi/app/src/Components/TechnicianList.js)
    * handles listing ALL technicians and delete one accordingly
  [TechnicianCreate](./ghi/app/src/Components/TechnicianCreate.js)
    * handles create new technician
